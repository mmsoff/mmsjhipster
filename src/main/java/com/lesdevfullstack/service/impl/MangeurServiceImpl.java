package com.lesdevfullstack.service.impl;

import com.lesdevfullstack.service.MangeurService;
import com.lesdevfullstack.domain.Mangeur;
import com.lesdevfullstack.repository.MangeurRepository;
import com.lesdevfullstack.service.dto.MangeurDTO;
import com.lesdevfullstack.service.mapper.MangeurMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Mangeur}.
 */
@Service
@Transactional
public class MangeurServiceImpl implements MangeurService {

    private final Logger log = LoggerFactory.getLogger(MangeurServiceImpl.class);

    private final MangeurRepository mangeurRepository;

    private final MangeurMapper mangeurMapper;

    public MangeurServiceImpl(MangeurRepository mangeurRepository, MangeurMapper mangeurMapper) {
        this.mangeurRepository = mangeurRepository;
        this.mangeurMapper = mangeurMapper;
    }

    @Override
    public MangeurDTO save(MangeurDTO mangeurDTO) {
        log.debug("Request to save Mangeur : {}", mangeurDTO);
        Mangeur mangeur = mangeurMapper.toEntity(mangeurDTO);
        mangeur = mangeurRepository.save(mangeur);
        return mangeurMapper.toDto(mangeur);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MangeurDTO> findAll() {
        log.debug("Request to get all Mangeurs");
        return mangeurRepository.findAll().stream()
            .map(mangeurMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<MangeurDTO> findOne(Long id) {
        log.debug("Request to get Mangeur : {}", id);
        return mangeurRepository.findById(id)
            .map(mangeurMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Mangeur : {}", id);
        mangeurRepository.deleteById(id);
    }
}
