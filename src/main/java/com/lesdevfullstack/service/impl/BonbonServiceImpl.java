package com.lesdevfullstack.service.impl;

import com.lesdevfullstack.service.BonbonService;
import com.lesdevfullstack.domain.Bonbon;
import com.lesdevfullstack.repository.BonbonRepository;
import com.lesdevfullstack.service.dto.BonbonDTO;
import com.lesdevfullstack.service.mapper.BonbonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Bonbon}.
 */
@Service
@Transactional
public class BonbonServiceImpl implements BonbonService {

    private final Logger log = LoggerFactory.getLogger(BonbonServiceImpl.class);

    private final BonbonRepository bonbonRepository;

    private final BonbonMapper bonbonMapper;

    public BonbonServiceImpl(BonbonRepository bonbonRepository, BonbonMapper bonbonMapper) {
        this.bonbonRepository = bonbonRepository;
        this.bonbonMapper = bonbonMapper;
    }

    @Override
    public BonbonDTO save(BonbonDTO bonbonDTO) {
        log.debug("Request to save Bonbon : {}", bonbonDTO);
        Bonbon bonbon = bonbonMapper.toEntity(bonbonDTO);
        bonbon = bonbonRepository.save(bonbon);
        return bonbonMapper.toDto(bonbon);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BonbonDTO> findAll() {
        log.debug("Request to get all Bonbons");
        return bonbonRepository.findAll().stream()
            .map(bonbonMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<BonbonDTO> findOne(Long id) {
        log.debug("Request to get Bonbon : {}", id);
        return bonbonRepository.findById(id)
            .map(bonbonMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Bonbon : {}", id);
        bonbonRepository.deleteById(id);
    }
}
