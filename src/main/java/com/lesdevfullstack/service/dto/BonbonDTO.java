package com.lesdevfullstack.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.lesdevfullstack.domain.Bonbon} entity.
 */
public class BonbonDTO implements Serializable {
    
    private Long id;

    private String marque;

    private String gtin;


    private Long mangeurId;

    private String mangeurPrenom;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getGtin() {
        return gtin;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public Long getMangeurId() {
        return mangeurId;
    }

    public void setMangeurId(Long mangeurId) {
        this.mangeurId = mangeurId;
    }

    public String getMangeurPrenom() {
        return mangeurPrenom;
    }

    public void setMangeurPrenom(String mangeurPrenom) {
        this.mangeurPrenom = mangeurPrenom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BonbonDTO)) {
            return false;
        }

        return id != null && id.equals(((BonbonDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BonbonDTO{" +
            "id=" + getId() +
            ", marque='" + getMarque() + "'" +
            ", gtin='" + getGtin() + "'" +
            ", mangeurId=" + getMangeurId() +
            ", mangeurPrenom='" + getMangeurPrenom() + "'" +
            "}";
    }
}
