package com.lesdevfullstack.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.lesdevfullstack.domain.Mangeur} entity.
 */
public class MangeurDTO implements Serializable {
    
    private Long id;

    private String prenom;

    private String nom;

    private Boolean estRadin;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Boolean isEstRadin() {
        return estRadin;
    }

    public void setEstRadin(Boolean estRadin) {
        this.estRadin = estRadin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MangeurDTO)) {
            return false;
        }

        return id != null && id.equals(((MangeurDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MangeurDTO{" +
            "id=" + getId() +
            ", prenom='" + getPrenom() + "'" +
            ", nom='" + getNom() + "'" +
            ", estRadin='" + isEstRadin() + "'" +
            "}";
    }
}
