package com.lesdevfullstack.service;

import com.lesdevfullstack.service.dto.BonbonDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.lesdevfullstack.domain.Bonbon}.
 */
public interface BonbonService {

    /**
     * Save a bonbon.
     *
     * @param bonbonDTO the entity to save.
     * @return the persisted entity.
     */
    BonbonDTO save(BonbonDTO bonbonDTO);

    /**
     * Get all the bonbons.
     *
     * @return the list of entities.
     */
    List<BonbonDTO> findAll();


    /**
     * Get the "id" bonbon.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BonbonDTO> findOne(Long id);

    /**
     * Delete the "id" bonbon.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
