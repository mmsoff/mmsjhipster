package com.lesdevfullstack.service.mapper;


import com.lesdevfullstack.domain.*;
import com.lesdevfullstack.service.dto.MangeurDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Mangeur} and its DTO {@link MangeurDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MangeurMapper extends EntityMapper<MangeurDTO, Mangeur> {



    default Mangeur fromId(Long id) {
        if (id == null) {
            return null;
        }
        Mangeur mangeur = new Mangeur();
        mangeur.setId(id);
        return mangeur;
    }
}
