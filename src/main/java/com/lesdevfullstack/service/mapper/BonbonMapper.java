package com.lesdevfullstack.service.mapper;


import com.lesdevfullstack.domain.*;
import com.lesdevfullstack.service.dto.BonbonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Bonbon} and its DTO {@link BonbonDTO}.
 */
@Mapper(componentModel = "spring", uses = {MangeurMapper.class})
public interface BonbonMapper extends EntityMapper<BonbonDTO, Bonbon> {

    @Mapping(source = "mangeur.id", target = "mangeurId")
    @Mapping(source = "mangeur.prenom", target = "mangeurPrenom")
    BonbonDTO toDto(Bonbon bonbon);

    @Mapping(source = "mangeurId", target = "mangeur")
    Bonbon toEntity(BonbonDTO bonbonDTO);

    default Bonbon fromId(Long id) {
        if (id == null) {
            return null;
        }
        Bonbon bonbon = new Bonbon();
        bonbon.setId(id);
        return bonbon;
    }
}
