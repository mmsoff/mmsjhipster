package com.lesdevfullstack.service;

import com.lesdevfullstack.service.dto.MangeurDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.lesdevfullstack.domain.Mangeur}.
 */
public interface MangeurService {

    /**
     * Save a mangeur.
     *
     * @param mangeurDTO the entity to save.
     * @return the persisted entity.
     */
    MangeurDTO save(MangeurDTO mangeurDTO);

    /**
     * Get all the mangeurs.
     *
     * @return the list of entities.
     */
    List<MangeurDTO> findAll();


    /**
     * Get the "id" mangeur.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MangeurDTO> findOne(Long id);

    /**
     * Delete the "id" mangeur.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
