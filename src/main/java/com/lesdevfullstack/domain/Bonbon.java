package com.lesdevfullstack.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Bonbon.
 */
@Entity
@Table(name = "bonbon")
public class Bonbon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "marque")
    private String marque;

    @Column(name = "gtin")
    private String gtin;

    @ManyToOne
    @JsonIgnoreProperties(value = "bonbons", allowSetters = true)
    private Mangeur mangeur;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMarque() {
        return marque;
    }

    public Bonbon marque(String marque) {
        this.marque = marque;
        return this;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getGtin() {
        return gtin;
    }

    public Bonbon gtin(String gtin) {
        this.gtin = gtin;
        return this;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public Mangeur getMangeur() {
        return mangeur;
    }

    public Bonbon mangeur(Mangeur mangeur) {
        this.mangeur = mangeur;
        return this;
    }

    public void setMangeur(Mangeur mangeur) {
        this.mangeur = mangeur;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Bonbon)) {
            return false;
        }
        return id != null && id.equals(((Bonbon) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Bonbon{" +
            "id=" + getId() +
            ", marque='" + getMarque() + "'" +
            ", gtin='" + getGtin() + "'" +
            "}";
    }
}
