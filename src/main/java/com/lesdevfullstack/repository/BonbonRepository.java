package com.lesdevfullstack.repository;

import com.lesdevfullstack.domain.Bonbon;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Bonbon entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BonbonRepository extends JpaRepository<Bonbon, Long> {
}
