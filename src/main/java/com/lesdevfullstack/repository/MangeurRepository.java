package com.lesdevfullstack.repository;

import com.lesdevfullstack.domain.Mangeur;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Mangeur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MangeurRepository extends JpaRepository<Mangeur, Long> {
}
