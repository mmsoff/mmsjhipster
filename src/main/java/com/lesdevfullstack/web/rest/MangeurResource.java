package com.lesdevfullstack.web.rest;

import com.lesdevfullstack.service.MangeurService;
import com.lesdevfullstack.web.rest.errors.BadRequestAlertException;
import com.lesdevfullstack.service.dto.MangeurDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.lesdevfullstack.domain.Mangeur}.
 */
@RestController
@RequestMapping("/api")
public class MangeurResource {

    private final Logger log = LoggerFactory.getLogger(MangeurResource.class);

    private static final String ENTITY_NAME = "mangeur";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MangeurService mangeurService;

    public MangeurResource(MangeurService mangeurService) {
        this.mangeurService = mangeurService;
    }

    /**
     * {@code POST  /mangeurs} : Create a new mangeur.
     *
     * @param mangeurDTO the mangeurDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mangeurDTO, or with status {@code 400 (Bad Request)} if the mangeur has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/mangeurs")
    public ResponseEntity<MangeurDTO> createMangeur(@RequestBody MangeurDTO mangeurDTO) throws URISyntaxException {
        log.debug("REST request to save Mangeur : {}", mangeurDTO);
        if (mangeurDTO.getId() != null) {
            throw new BadRequestAlertException("A new mangeur cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MangeurDTO result = mangeurService.save(mangeurDTO);
        return ResponseEntity.created(new URI("/api/mangeurs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mangeurs} : Updates an existing mangeur.
     *
     * @param mangeurDTO the mangeurDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mangeurDTO,
     * or with status {@code 400 (Bad Request)} if the mangeurDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the mangeurDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mangeurs")
    public ResponseEntity<MangeurDTO> updateMangeur(@RequestBody MangeurDTO mangeurDTO) throws URISyntaxException {
        log.debug("REST request to update Mangeur : {}", mangeurDTO);
        if (mangeurDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MangeurDTO result = mangeurService.save(mangeurDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, mangeurDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /mangeurs} : get all the mangeurs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mangeurs in body.
     */
    @GetMapping("/mangeurs")
    public List<MangeurDTO> getAllMangeurs() {
        log.debug("REST request to get all Mangeurs");
        return mangeurService.findAll();
    }

    /**
     * {@code GET  /mangeurs/:id} : get the "id" mangeur.
     *
     * @param id the id of the mangeurDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mangeurDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mangeurs/{id}")
    public ResponseEntity<MangeurDTO> getMangeur(@PathVariable Long id) {
        log.debug("REST request to get Mangeur : {}", id);
        Optional<MangeurDTO> mangeurDTO = mangeurService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mangeurDTO);
    }

    /**
     * {@code DELETE  /mangeurs/:id} : delete the "id" mangeur.
     *
     * @param id the id of the mangeurDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mangeurs/{id}")
    public ResponseEntity<Void> deleteMangeur(@PathVariable Long id) {
        log.debug("REST request to delete Mangeur : {}", id);
        mangeurService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
