package com.lesdevfullstack.web.rest;

import com.lesdevfullstack.service.BonbonService;
import com.lesdevfullstack.web.rest.errors.BadRequestAlertException;
import com.lesdevfullstack.service.dto.BonbonDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.lesdevfullstack.domain.Bonbon}.
 */
@RestController
@RequestMapping("/api")
public class BonbonResource {

    private final Logger log = LoggerFactory.getLogger(BonbonResource.class);

    private static final String ENTITY_NAME = "bonbon";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BonbonService bonbonService;

    public BonbonResource(BonbonService bonbonService) {
        this.bonbonService = bonbonService;
    }

    /**
     * {@code POST  /bonbons} : Create a new bonbon.
     *
     * @param bonbonDTO the bonbonDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bonbonDTO, or with status {@code 400 (Bad Request)} if the bonbon has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bonbons")
    public ResponseEntity<BonbonDTO> createBonbon(@RequestBody BonbonDTO bonbonDTO) throws URISyntaxException {
        log.debug("REST request to save Bonbon : {}", bonbonDTO);
        if (bonbonDTO.getId() != null) {
            throw new BadRequestAlertException("A new bonbon cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BonbonDTO result = bonbonService.save(bonbonDTO);
        return ResponseEntity.created(new URI("/api/bonbons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bonbons} : Updates an existing bonbon.
     *
     * @param bonbonDTO the bonbonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bonbonDTO,
     * or with status {@code 400 (Bad Request)} if the bonbonDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bonbonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bonbons")
    public ResponseEntity<BonbonDTO> updateBonbon(@RequestBody BonbonDTO bonbonDTO) throws URISyntaxException {
        log.debug("REST request to update Bonbon : {}", bonbonDTO);
        if (bonbonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BonbonDTO result = bonbonService.save(bonbonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, bonbonDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bonbons} : get all the bonbons.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bonbons in body.
     */
    @GetMapping("/bonbons")
    public List<BonbonDTO> getAllBonbons() {
        log.debug("REST request to get all Bonbons");
        return bonbonService.findAll();
    }

    /**
     * {@code GET  /bonbons/:id} : get the "id" bonbon.
     *
     * @param id the id of the bonbonDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bonbonDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bonbons/{id}")
    public ResponseEntity<BonbonDTO> getBonbon(@PathVariable Long id) {
        log.debug("REST request to get Bonbon : {}", id);
        Optional<BonbonDTO> bonbonDTO = bonbonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bonbonDTO);
    }

    /**
     * {@code DELETE  /bonbons/:id} : delete the "id" bonbon.
     *
     * @param id the id of the bonbonDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bonbons/{id}")
    public ResponseEntity<Void> deleteBonbon(@PathVariable Long id) {
        log.debug("REST request to delete Bonbon : {}", id);
        bonbonService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
