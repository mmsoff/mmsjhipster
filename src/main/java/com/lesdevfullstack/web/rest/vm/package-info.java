/**
 * View Models used by Spring MVC REST controllers.
 */
package com.lesdevfullstack.web.rest.vm;
