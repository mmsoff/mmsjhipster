import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { MmsSharedModule } from 'app/shared/shared.module';
import { MmsCoreModule } from 'app/core/core.module';
import { MmsAppRoutingModule } from './app-routing.module';
import { MmsHomeModule } from './home/home.module';
import { MmsEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    MmsSharedModule,
    MmsCoreModule,
    MmsHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    MmsEntityModule,
    MmsAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class MmsAppModule {}
