import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IMangeur } from 'app/shared/model/mangeur.model';

type EntityResponseType = HttpResponse<IMangeur>;
type EntityArrayResponseType = HttpResponse<IMangeur[]>;

@Injectable({ providedIn: 'root' })
export class MangeurService {
  public resourceUrl = SERVER_API_URL + 'api/mangeurs';

  constructor(protected http: HttpClient) {}

  create(mangeur: IMangeur): Observable<EntityResponseType> {
    return this.http.post<IMangeur>(this.resourceUrl, mangeur, { observe: 'response' });
  }

  update(mangeur: IMangeur): Observable<EntityResponseType> {
    return this.http.put<IMangeur>(this.resourceUrl, mangeur, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMangeur>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMangeur[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
