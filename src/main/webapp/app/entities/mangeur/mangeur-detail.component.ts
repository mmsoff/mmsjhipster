import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMangeur } from 'app/shared/model/mangeur.model';

@Component({
  selector: 'jhi-mangeur-detail',
  templateUrl: './mangeur-detail.component.html',
})
export class MangeurDetailComponent implements OnInit {
  mangeur: IMangeur | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mangeur }) => (this.mangeur = mangeur));
  }

  previousState(): void {
    window.history.back();
  }
}
