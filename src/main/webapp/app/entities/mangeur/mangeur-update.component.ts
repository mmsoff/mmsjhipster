import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IMangeur, Mangeur } from 'app/shared/model/mangeur.model';
import { MangeurService } from './mangeur.service';

@Component({
  selector: 'jhi-mangeur-update',
  templateUrl: './mangeur-update.component.html',
})
export class MangeurUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    prenom: [],
    nom: [],
    estRadin: [],
  });

  constructor(protected mangeurService: MangeurService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mangeur }) => {
      this.updateForm(mangeur);
    });
  }

  updateForm(mangeur: IMangeur): void {
    this.editForm.patchValue({
      id: mangeur.id,
      prenom: mangeur.prenom,
      nom: mangeur.nom,
      estRadin: mangeur.estRadin,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const mangeur = this.createFromForm();
    if (mangeur.id !== undefined) {
      this.subscribeToSaveResponse(this.mangeurService.update(mangeur));
    } else {
      this.subscribeToSaveResponse(this.mangeurService.create(mangeur));
    }
  }

  private createFromForm(): IMangeur {
    return {
      ...new Mangeur(),
      id: this.editForm.get(['id'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      estRadin: this.editForm.get(['estRadin'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMangeur>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
