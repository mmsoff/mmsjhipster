import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IMangeur, Mangeur } from 'app/shared/model/mangeur.model';
import { MangeurService } from './mangeur.service';
import { MangeurComponent } from './mangeur.component';
import { MangeurDetailComponent } from './mangeur-detail.component';
import { MangeurUpdateComponent } from './mangeur-update.component';

@Injectable({ providedIn: 'root' })
export class MangeurResolve implements Resolve<IMangeur> {
  constructor(private service: MangeurService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMangeur> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((mangeur: HttpResponse<Mangeur>) => {
          if (mangeur.body) {
            return of(mangeur.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Mangeur());
  }
}

export const mangeurRoute: Routes = [
  {
    path: '',
    component: MangeurComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Mangeurs',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MangeurDetailComponent,
    resolve: {
      mangeur: MangeurResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Mangeurs',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MangeurUpdateComponent,
    resolve: {
      mangeur: MangeurResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Mangeurs',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MangeurUpdateComponent,
    resolve: {
      mangeur: MangeurResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Mangeurs',
    },
    canActivate: [UserRouteAccessService],
  },
];
