import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMangeur } from 'app/shared/model/mangeur.model';
import { MangeurService } from './mangeur.service';
import { MangeurDeleteDialogComponent } from './mangeur-delete-dialog.component';

@Component({
  selector: 'jhi-mangeur',
  templateUrl: './mangeur.component.html',
})
export class MangeurComponent implements OnInit, OnDestroy {
  mangeurs?: IMangeur[];
  eventSubscriber?: Subscription;

  constructor(protected mangeurService: MangeurService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.mangeurService.query().subscribe((res: HttpResponse<IMangeur[]>) => (this.mangeurs = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInMangeurs();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IMangeur): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInMangeurs(): void {
    this.eventSubscriber = this.eventManager.subscribe('mangeurListModification', () => this.loadAll());
  }

  delete(mangeur: IMangeur): void {
    const modalRef = this.modalService.open(MangeurDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.mangeur = mangeur;
  }
}
