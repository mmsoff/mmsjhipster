import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MmsSharedModule } from 'app/shared/shared.module';
import { MangeurComponent } from './mangeur.component';
import { MangeurDetailComponent } from './mangeur-detail.component';
import { MangeurUpdateComponent } from './mangeur-update.component';
import { MangeurDeleteDialogComponent } from './mangeur-delete-dialog.component';
import { mangeurRoute } from './mangeur.route';

@NgModule({
  imports: [MmsSharedModule, RouterModule.forChild(mangeurRoute)],
  declarations: [MangeurComponent, MangeurDetailComponent, MangeurUpdateComponent, MangeurDeleteDialogComponent],
  entryComponents: [MangeurDeleteDialogComponent],
})
export class MmsMangeurModule {}
