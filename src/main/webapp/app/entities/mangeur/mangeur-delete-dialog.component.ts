import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMangeur } from 'app/shared/model/mangeur.model';
import { MangeurService } from './mangeur.service';

@Component({
  templateUrl: './mangeur-delete-dialog.component.html',
})
export class MangeurDeleteDialogComponent {
  mangeur?: IMangeur;

  constructor(protected mangeurService: MangeurService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.mangeurService.delete(id).subscribe(() => {
      this.eventManager.broadcast('mangeurListModification');
      this.activeModal.close();
    });
  }
}
