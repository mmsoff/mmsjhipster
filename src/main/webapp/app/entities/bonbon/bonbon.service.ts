import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBonbon } from 'app/shared/model/bonbon.model';

type EntityResponseType = HttpResponse<IBonbon>;
type EntityArrayResponseType = HttpResponse<IBonbon[]>;

@Injectable({ providedIn: 'root' })
export class BonbonService {
  public resourceUrl = SERVER_API_URL + 'api/bonbons';

  constructor(protected http: HttpClient) {}

  create(bonbon: IBonbon): Observable<EntityResponseType> {
    return this.http.post<IBonbon>(this.resourceUrl, bonbon, { observe: 'response' });
  }

  update(bonbon: IBonbon): Observable<EntityResponseType> {
    return this.http.put<IBonbon>(this.resourceUrl, bonbon, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBonbon>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBonbon[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
