import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBonbon, Bonbon } from 'app/shared/model/bonbon.model';
import { BonbonService } from './bonbon.service';
import { IMangeur } from 'app/shared/model/mangeur.model';
import { MangeurService } from 'app/entities/mangeur/mangeur.service';

@Component({
  selector: 'jhi-bonbon-update',
  templateUrl: './bonbon-update.component.html',
})
export class BonbonUpdateComponent implements OnInit {
  isSaving = false;
  mangeurs: IMangeur[] = [];

  editForm = this.fb.group({
    id: [],
    marque: [],
    gtin: [],
    mangeurId: [],
  });

  constructor(
    protected bonbonService: BonbonService,
    protected mangeurService: MangeurService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bonbon }) => {
      this.updateForm(bonbon);

      this.mangeurService.query().subscribe((res: HttpResponse<IMangeur[]>) => (this.mangeurs = res.body || []));
    });
  }

  updateForm(bonbon: IBonbon): void {
    this.editForm.patchValue({
      id: bonbon.id,
      marque: bonbon.marque,
      gtin: bonbon.gtin,
      mangeurId: bonbon.mangeurId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bonbon = this.createFromForm();
    if (bonbon.id !== undefined) {
      this.subscribeToSaveResponse(this.bonbonService.update(bonbon));
    } else {
      this.subscribeToSaveResponse(this.bonbonService.create(bonbon));
    }
  }

  private createFromForm(): IBonbon {
    return {
      ...new Bonbon(),
      id: this.editForm.get(['id'])!.value,
      marque: this.editForm.get(['marque'])!.value,
      gtin: this.editForm.get(['gtin'])!.value,
      mangeurId: this.editForm.get(['mangeurId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBonbon>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IMangeur): any {
    return item.id;
  }
}
