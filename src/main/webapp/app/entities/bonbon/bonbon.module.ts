import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MmsSharedModule } from 'app/shared/shared.module';
import { BonbonComponent } from './bonbon.component';
import { BonbonDetailComponent } from './bonbon-detail.component';
import { BonbonUpdateComponent } from './bonbon-update.component';
import { BonbonDeleteDialogComponent } from './bonbon-delete-dialog.component';
import { bonbonRoute } from './bonbon.route';

@NgModule({
  imports: [MmsSharedModule, RouterModule.forChild(bonbonRoute)],
  declarations: [BonbonComponent, BonbonDetailComponent, BonbonUpdateComponent, BonbonDeleteDialogComponent],
  entryComponents: [BonbonDeleteDialogComponent],
})
export class MmsBonbonModule {}
