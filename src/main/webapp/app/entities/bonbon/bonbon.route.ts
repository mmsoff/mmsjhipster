import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBonbon, Bonbon } from 'app/shared/model/bonbon.model';
import { BonbonService } from './bonbon.service';
import { BonbonComponent } from './bonbon.component';
import { BonbonDetailComponent } from './bonbon-detail.component';
import { BonbonUpdateComponent } from './bonbon-update.component';

@Injectable({ providedIn: 'root' })
export class BonbonResolve implements Resolve<IBonbon> {
  constructor(private service: BonbonService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBonbon> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bonbon: HttpResponse<Bonbon>) => {
          if (bonbon.body) {
            return of(bonbon.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Bonbon());
  }
}

export const bonbonRoute: Routes = [
  {
    path: '',
    component: BonbonComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Bonbons',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BonbonDetailComponent,
    resolve: {
      bonbon: BonbonResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Bonbons',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BonbonUpdateComponent,
    resolve: {
      bonbon: BonbonResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Bonbons',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BonbonUpdateComponent,
    resolve: {
      bonbon: BonbonResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Bonbons',
    },
    canActivate: [UserRouteAccessService],
  },
];
