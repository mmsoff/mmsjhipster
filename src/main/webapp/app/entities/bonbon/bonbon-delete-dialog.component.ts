import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBonbon } from 'app/shared/model/bonbon.model';
import { BonbonService } from './bonbon.service';

@Component({
  templateUrl: './bonbon-delete-dialog.component.html',
})
export class BonbonDeleteDialogComponent {
  bonbon?: IBonbon;

  constructor(protected bonbonService: BonbonService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.bonbonService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bonbonListModification');
      this.activeModal.close();
    });
  }
}
