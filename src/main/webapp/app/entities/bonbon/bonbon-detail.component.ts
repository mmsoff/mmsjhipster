import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBonbon } from 'app/shared/model/bonbon.model';

@Component({
  selector: 'jhi-bonbon-detail',
  templateUrl: './bonbon-detail.component.html',
})
export class BonbonDetailComponent implements OnInit {
  bonbon: IBonbon | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bonbon }) => (this.bonbon = bonbon));
  }

  previousState(): void {
    window.history.back();
  }
}
