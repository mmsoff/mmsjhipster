import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBonbon } from 'app/shared/model/bonbon.model';
import { BonbonService } from './bonbon.service';
import { BonbonDeleteDialogComponent } from './bonbon-delete-dialog.component';

@Component({
  selector: 'jhi-bonbon',
  templateUrl: './bonbon.component.html',
})
export class BonbonComponent implements OnInit, OnDestroy {
  bonbons?: IBonbon[];
  eventSubscriber?: Subscription;

  constructor(protected bonbonService: BonbonService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.bonbonService.query().subscribe((res: HttpResponse<IBonbon[]>) => (this.bonbons = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBonbons();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBonbon): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBonbons(): void {
    this.eventSubscriber = this.eventManager.subscribe('bonbonListModification', () => this.loadAll());
  }

  delete(bonbon: IBonbon): void {
    const modalRef = this.modalService.open(BonbonDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.bonbon = bonbon;
  }
}
