import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'mangeur',
        loadChildren: () => import('./mangeur/mangeur.module').then(m => m.MmsMangeurModule),
      },
      {
        path: 'bonbon',
        loadChildren: () => import('./bonbon/bonbon.module').then(m => m.MmsBonbonModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class MmsEntityModule {}
