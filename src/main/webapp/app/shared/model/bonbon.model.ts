export interface IBonbon {
  id?: number;
  marque?: string;
  gtin?: string;
  mangeurPrenom?: string;
  mangeurId?: number;
}

export class Bonbon implements IBonbon {
  constructor(public id?: number, public marque?: string, public gtin?: string, public mangeurPrenom?: string, public mangeurId?: number) {}
}
