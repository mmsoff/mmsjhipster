export interface IMangeur {
  id?: number;
  prenom?: string;
  nom?: string;
  estRadin?: boolean;
}

export class Mangeur implements IMangeur {
  constructor(public id?: number, public prenom?: string, public nom?: string, public estRadin?: boolean) {
    this.estRadin = this.estRadin || false;
  }
}
