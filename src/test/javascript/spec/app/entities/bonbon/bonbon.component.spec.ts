import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MmsTestModule } from '../../../test.module';
import { BonbonComponent } from 'app/entities/bonbon/bonbon.component';
import { BonbonService } from 'app/entities/bonbon/bonbon.service';
import { Bonbon } from 'app/shared/model/bonbon.model';

describe('Component Tests', () => {
  describe('Bonbon Management Component', () => {
    let comp: BonbonComponent;
    let fixture: ComponentFixture<BonbonComponent>;
    let service: BonbonService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MmsTestModule],
        declarations: [BonbonComponent],
      })
        .overrideTemplate(BonbonComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BonbonComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BonbonService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Bonbon(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bonbons && comp.bonbons[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
