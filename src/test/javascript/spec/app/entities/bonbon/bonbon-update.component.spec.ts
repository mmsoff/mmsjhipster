import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MmsTestModule } from '../../../test.module';
import { BonbonUpdateComponent } from 'app/entities/bonbon/bonbon-update.component';
import { BonbonService } from 'app/entities/bonbon/bonbon.service';
import { Bonbon } from 'app/shared/model/bonbon.model';

describe('Component Tests', () => {
  describe('Bonbon Management Update Component', () => {
    let comp: BonbonUpdateComponent;
    let fixture: ComponentFixture<BonbonUpdateComponent>;
    let service: BonbonService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MmsTestModule],
        declarations: [BonbonUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BonbonUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BonbonUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BonbonService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Bonbon(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Bonbon();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
