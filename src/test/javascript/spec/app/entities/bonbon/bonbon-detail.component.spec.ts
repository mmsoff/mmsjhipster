import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MmsTestModule } from '../../../test.module';
import { BonbonDetailComponent } from 'app/entities/bonbon/bonbon-detail.component';
import { Bonbon } from 'app/shared/model/bonbon.model';

describe('Component Tests', () => {
  describe('Bonbon Management Detail Component', () => {
    let comp: BonbonDetailComponent;
    let fixture: ComponentFixture<BonbonDetailComponent>;
    const route = ({ data: of({ bonbon: new Bonbon(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MmsTestModule],
        declarations: [BonbonDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BonbonDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BonbonDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bonbon on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bonbon).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
