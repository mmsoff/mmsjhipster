import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MmsTestModule } from '../../../test.module';
import { MangeurUpdateComponent } from 'app/entities/mangeur/mangeur-update.component';
import { MangeurService } from 'app/entities/mangeur/mangeur.service';
import { Mangeur } from 'app/shared/model/mangeur.model';

describe('Component Tests', () => {
  describe('Mangeur Management Update Component', () => {
    let comp: MangeurUpdateComponent;
    let fixture: ComponentFixture<MangeurUpdateComponent>;
    let service: MangeurService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MmsTestModule],
        declarations: [MangeurUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(MangeurUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MangeurUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MangeurService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Mangeur(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Mangeur();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
