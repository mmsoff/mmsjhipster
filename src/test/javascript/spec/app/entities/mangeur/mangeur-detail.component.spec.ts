import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MmsTestModule } from '../../../test.module';
import { MangeurDetailComponent } from 'app/entities/mangeur/mangeur-detail.component';
import { Mangeur } from 'app/shared/model/mangeur.model';

describe('Component Tests', () => {
  describe('Mangeur Management Detail Component', () => {
    let comp: MangeurDetailComponent;
    let fixture: ComponentFixture<MangeurDetailComponent>;
    const route = ({ data: of({ mangeur: new Mangeur(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MmsTestModule],
        declarations: [MangeurDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(MangeurDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MangeurDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load mangeur on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.mangeur).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
