import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MmsTestModule } from '../../../test.module';
import { MangeurComponent } from 'app/entities/mangeur/mangeur.component';
import { MangeurService } from 'app/entities/mangeur/mangeur.service';
import { Mangeur } from 'app/shared/model/mangeur.model';

describe('Component Tests', () => {
  describe('Mangeur Management Component', () => {
    let comp: MangeurComponent;
    let fixture: ComponentFixture<MangeurComponent>;
    let service: MangeurService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MmsTestModule],
        declarations: [MangeurComponent],
      })
        .overrideTemplate(MangeurComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MangeurComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MangeurService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Mangeur(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.mangeurs && comp.mangeurs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
