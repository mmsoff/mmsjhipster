package com.lesdevfullstack.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.lesdevfullstack.web.rest.TestUtil;

public class BonbonTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Bonbon.class);
        Bonbon bonbon1 = new Bonbon();
        bonbon1.setId(1L);
        Bonbon bonbon2 = new Bonbon();
        bonbon2.setId(bonbon1.getId());
        assertThat(bonbon1).isEqualTo(bonbon2);
        bonbon2.setId(2L);
        assertThat(bonbon1).isNotEqualTo(bonbon2);
        bonbon1.setId(null);
        assertThat(bonbon1).isNotEqualTo(bonbon2);
    }
}
