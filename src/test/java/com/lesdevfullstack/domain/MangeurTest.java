package com.lesdevfullstack.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.lesdevfullstack.web.rest.TestUtil;

public class MangeurTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Mangeur.class);
        Mangeur mangeur1 = new Mangeur();
        mangeur1.setId(1L);
        Mangeur mangeur2 = new Mangeur();
        mangeur2.setId(mangeur1.getId());
        assertThat(mangeur1).isEqualTo(mangeur2);
        mangeur2.setId(2L);
        assertThat(mangeur1).isNotEqualTo(mangeur2);
        mangeur1.setId(null);
        assertThat(mangeur1).isNotEqualTo(mangeur2);
    }
}
