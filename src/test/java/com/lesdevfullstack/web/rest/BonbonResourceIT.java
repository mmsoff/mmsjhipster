package com.lesdevfullstack.web.rest;

import com.lesdevfullstack.MmsApp;
import com.lesdevfullstack.domain.Bonbon;
import com.lesdevfullstack.repository.BonbonRepository;
import com.lesdevfullstack.service.BonbonService;
import com.lesdevfullstack.service.dto.BonbonDTO;
import com.lesdevfullstack.service.mapper.BonbonMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BonbonResource} REST controller.
 */
@SpringBootTest(classes = MmsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BonbonResourceIT {

    private static final String DEFAULT_MARQUE = "AAAAAAAAAA";
    private static final String UPDATED_MARQUE = "BBBBBBBBBB";

    private static final String DEFAULT_GTIN = "AAAAAAAAAA";
    private static final String UPDATED_GTIN = "BBBBBBBBBB";

    @Autowired
    private BonbonRepository bonbonRepository;

    @Autowired
    private BonbonMapper bonbonMapper;

    @Autowired
    private BonbonService bonbonService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBonbonMockMvc;

    private Bonbon bonbon;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bonbon createEntity(EntityManager em) {
        Bonbon bonbon = new Bonbon()
            .marque(DEFAULT_MARQUE)
            .gtin(DEFAULT_GTIN);
        return bonbon;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bonbon createUpdatedEntity(EntityManager em) {
        Bonbon bonbon = new Bonbon()
            .marque(UPDATED_MARQUE)
            .gtin(UPDATED_GTIN);
        return bonbon;
    }

    @BeforeEach
    public void initTest() {
        bonbon = createEntity(em);
    }

    @Test
    @Transactional
    public void createBonbon() throws Exception {
        int databaseSizeBeforeCreate = bonbonRepository.findAll().size();
        // Create the Bonbon
        BonbonDTO bonbonDTO = bonbonMapper.toDto(bonbon);
        restBonbonMockMvc.perform(post("/api/bonbons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bonbonDTO)))
            .andExpect(status().isCreated());

        // Validate the Bonbon in the database
        List<Bonbon> bonbonList = bonbonRepository.findAll();
        assertThat(bonbonList).hasSize(databaseSizeBeforeCreate + 1);
        Bonbon testBonbon = bonbonList.get(bonbonList.size() - 1);
        assertThat(testBonbon.getMarque()).isEqualTo(DEFAULT_MARQUE);
        assertThat(testBonbon.getGtin()).isEqualTo(DEFAULT_GTIN);
    }

    @Test
    @Transactional
    public void createBonbonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bonbonRepository.findAll().size();

        // Create the Bonbon with an existing ID
        bonbon.setId(1L);
        BonbonDTO bonbonDTO = bonbonMapper.toDto(bonbon);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBonbonMockMvc.perform(post("/api/bonbons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bonbonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Bonbon in the database
        List<Bonbon> bonbonList = bonbonRepository.findAll();
        assertThat(bonbonList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBonbons() throws Exception {
        // Initialize the database
        bonbonRepository.saveAndFlush(bonbon);

        // Get all the bonbonList
        restBonbonMockMvc.perform(get("/api/bonbons?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bonbon.getId().intValue())))
            .andExpect(jsonPath("$.[*].marque").value(hasItem(DEFAULT_MARQUE)))
            .andExpect(jsonPath("$.[*].gtin").value(hasItem(DEFAULT_GTIN)));
    }
    
    @Test
    @Transactional
    public void getBonbon() throws Exception {
        // Initialize the database
        bonbonRepository.saveAndFlush(bonbon);

        // Get the bonbon
        restBonbonMockMvc.perform(get("/api/bonbons/{id}", bonbon.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bonbon.getId().intValue()))
            .andExpect(jsonPath("$.marque").value(DEFAULT_MARQUE))
            .andExpect(jsonPath("$.gtin").value(DEFAULT_GTIN));
    }
    @Test
    @Transactional
    public void getNonExistingBonbon() throws Exception {
        // Get the bonbon
        restBonbonMockMvc.perform(get("/api/bonbons/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBonbon() throws Exception {
        // Initialize the database
        bonbonRepository.saveAndFlush(bonbon);

        int databaseSizeBeforeUpdate = bonbonRepository.findAll().size();

        // Update the bonbon
        Bonbon updatedBonbon = bonbonRepository.findById(bonbon.getId()).get();
        // Disconnect from session so that the updates on updatedBonbon are not directly saved in db
        em.detach(updatedBonbon);
        updatedBonbon
            .marque(UPDATED_MARQUE)
            .gtin(UPDATED_GTIN);
        BonbonDTO bonbonDTO = bonbonMapper.toDto(updatedBonbon);

        restBonbonMockMvc.perform(put("/api/bonbons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bonbonDTO)))
            .andExpect(status().isOk());

        // Validate the Bonbon in the database
        List<Bonbon> bonbonList = bonbonRepository.findAll();
        assertThat(bonbonList).hasSize(databaseSizeBeforeUpdate);
        Bonbon testBonbon = bonbonList.get(bonbonList.size() - 1);
        assertThat(testBonbon.getMarque()).isEqualTo(UPDATED_MARQUE);
        assertThat(testBonbon.getGtin()).isEqualTo(UPDATED_GTIN);
    }

    @Test
    @Transactional
    public void updateNonExistingBonbon() throws Exception {
        int databaseSizeBeforeUpdate = bonbonRepository.findAll().size();

        // Create the Bonbon
        BonbonDTO bonbonDTO = bonbonMapper.toDto(bonbon);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBonbonMockMvc.perform(put("/api/bonbons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bonbonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Bonbon in the database
        List<Bonbon> bonbonList = bonbonRepository.findAll();
        assertThat(bonbonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBonbon() throws Exception {
        // Initialize the database
        bonbonRepository.saveAndFlush(bonbon);

        int databaseSizeBeforeDelete = bonbonRepository.findAll().size();

        // Delete the bonbon
        restBonbonMockMvc.perform(delete("/api/bonbons/{id}", bonbon.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Bonbon> bonbonList = bonbonRepository.findAll();
        assertThat(bonbonList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
