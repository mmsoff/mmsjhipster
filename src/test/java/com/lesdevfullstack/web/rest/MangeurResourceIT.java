package com.lesdevfullstack.web.rest;

import com.lesdevfullstack.MmsApp;
import com.lesdevfullstack.domain.Mangeur;
import com.lesdevfullstack.repository.MangeurRepository;
import com.lesdevfullstack.service.MangeurService;
import com.lesdevfullstack.service.dto.MangeurDTO;
import com.lesdevfullstack.service.mapper.MangeurMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MangeurResource} REST controller.
 */
@SpringBootTest(classes = MmsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class MangeurResourceIT {

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final Boolean DEFAULT_EST_RADIN = false;
    private static final Boolean UPDATED_EST_RADIN = true;

    @Autowired
    private MangeurRepository mangeurRepository;

    @Autowired
    private MangeurMapper mangeurMapper;

    @Autowired
    private MangeurService mangeurService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMangeurMockMvc;

    private Mangeur mangeur;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Mangeur createEntity(EntityManager em) {
        Mangeur mangeur = new Mangeur()
            .prenom(DEFAULT_PRENOM)
            .nom(DEFAULT_NOM)
            .estRadin(DEFAULT_EST_RADIN);
        return mangeur;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Mangeur createUpdatedEntity(EntityManager em) {
        Mangeur mangeur = new Mangeur()
            .prenom(UPDATED_PRENOM)
            .nom(UPDATED_NOM)
            .estRadin(UPDATED_EST_RADIN);
        return mangeur;
    }

    @BeforeEach
    public void initTest() {
        mangeur = createEntity(em);
    }

    @Test
    @Transactional
    public void createMangeur() throws Exception {
        int databaseSizeBeforeCreate = mangeurRepository.findAll().size();
        // Create the Mangeur
        MangeurDTO mangeurDTO = mangeurMapper.toDto(mangeur);
        restMangeurMockMvc.perform(post("/api/mangeurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mangeurDTO)))
            .andExpect(status().isCreated());

        // Validate the Mangeur in the database
        List<Mangeur> mangeurList = mangeurRepository.findAll();
        assertThat(mangeurList).hasSize(databaseSizeBeforeCreate + 1);
        Mangeur testMangeur = mangeurList.get(mangeurList.size() - 1);
        assertThat(testMangeur.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testMangeur.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testMangeur.isEstRadin()).isEqualTo(DEFAULT_EST_RADIN);
    }

    @Test
    @Transactional
    public void createMangeurWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = mangeurRepository.findAll().size();

        // Create the Mangeur with an existing ID
        mangeur.setId(1L);
        MangeurDTO mangeurDTO = mangeurMapper.toDto(mangeur);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMangeurMockMvc.perform(post("/api/mangeurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mangeurDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Mangeur in the database
        List<Mangeur> mangeurList = mangeurRepository.findAll();
        assertThat(mangeurList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMangeurs() throws Exception {
        // Initialize the database
        mangeurRepository.saveAndFlush(mangeur);

        // Get all the mangeurList
        restMangeurMockMvc.perform(get("/api/mangeurs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mangeur.getId().intValue())))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].estRadin").value(hasItem(DEFAULT_EST_RADIN.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getMangeur() throws Exception {
        // Initialize the database
        mangeurRepository.saveAndFlush(mangeur);

        // Get the mangeur
        restMangeurMockMvc.perform(get("/api/mangeurs/{id}", mangeur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(mangeur.getId().intValue()))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.estRadin").value(DEFAULT_EST_RADIN.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingMangeur() throws Exception {
        // Get the mangeur
        restMangeurMockMvc.perform(get("/api/mangeurs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMangeur() throws Exception {
        // Initialize the database
        mangeurRepository.saveAndFlush(mangeur);

        int databaseSizeBeforeUpdate = mangeurRepository.findAll().size();

        // Update the mangeur
        Mangeur updatedMangeur = mangeurRepository.findById(mangeur.getId()).get();
        // Disconnect from session so that the updates on updatedMangeur are not directly saved in db
        em.detach(updatedMangeur);
        updatedMangeur
            .prenom(UPDATED_PRENOM)
            .nom(UPDATED_NOM)
            .estRadin(UPDATED_EST_RADIN);
        MangeurDTO mangeurDTO = mangeurMapper.toDto(updatedMangeur);

        restMangeurMockMvc.perform(put("/api/mangeurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mangeurDTO)))
            .andExpect(status().isOk());

        // Validate the Mangeur in the database
        List<Mangeur> mangeurList = mangeurRepository.findAll();
        assertThat(mangeurList).hasSize(databaseSizeBeforeUpdate);
        Mangeur testMangeur = mangeurList.get(mangeurList.size() - 1);
        assertThat(testMangeur.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testMangeur.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testMangeur.isEstRadin()).isEqualTo(UPDATED_EST_RADIN);
    }

    @Test
    @Transactional
    public void updateNonExistingMangeur() throws Exception {
        int databaseSizeBeforeUpdate = mangeurRepository.findAll().size();

        // Create the Mangeur
        MangeurDTO mangeurDTO = mangeurMapper.toDto(mangeur);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMangeurMockMvc.perform(put("/api/mangeurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mangeurDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Mangeur in the database
        List<Mangeur> mangeurList = mangeurRepository.findAll();
        assertThat(mangeurList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMangeur() throws Exception {
        // Initialize the database
        mangeurRepository.saveAndFlush(mangeur);

        int databaseSizeBeforeDelete = mangeurRepository.findAll().size();

        // Delete the mangeur
        restMangeurMockMvc.perform(delete("/api/mangeurs/{id}", mangeur.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Mangeur> mangeurList = mangeurRepository.findAll();
        assertThat(mangeurList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
