package com.lesdevfullstack.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BonbonMapperTest {

    private BonbonMapper bonbonMapper;

    @BeforeEach
    public void setUp() {
        bonbonMapper = new BonbonMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(bonbonMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(bonbonMapper.fromId(null)).isNull();
    }
}
