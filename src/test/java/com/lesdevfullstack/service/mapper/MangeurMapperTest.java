package com.lesdevfullstack.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class MangeurMapperTest {

    private MangeurMapper mangeurMapper;

    @BeforeEach
    public void setUp() {
        mangeurMapper = new MangeurMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(mangeurMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(mangeurMapper.fromId(null)).isNull();
    }
}
