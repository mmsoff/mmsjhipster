package com.lesdevfullstack.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.lesdevfullstack.web.rest.TestUtil;

public class MangeurDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MangeurDTO.class);
        MangeurDTO mangeurDTO1 = new MangeurDTO();
        mangeurDTO1.setId(1L);
        MangeurDTO mangeurDTO2 = new MangeurDTO();
        assertThat(mangeurDTO1).isNotEqualTo(mangeurDTO2);
        mangeurDTO2.setId(mangeurDTO1.getId());
        assertThat(mangeurDTO1).isEqualTo(mangeurDTO2);
        mangeurDTO2.setId(2L);
        assertThat(mangeurDTO1).isNotEqualTo(mangeurDTO2);
        mangeurDTO1.setId(null);
        assertThat(mangeurDTO1).isNotEqualTo(mangeurDTO2);
    }
}
