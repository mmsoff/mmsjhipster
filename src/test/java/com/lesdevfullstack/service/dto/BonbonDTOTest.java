package com.lesdevfullstack.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.lesdevfullstack.web.rest.TestUtil;

public class BonbonDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BonbonDTO.class);
        BonbonDTO bonbonDTO1 = new BonbonDTO();
        bonbonDTO1.setId(1L);
        BonbonDTO bonbonDTO2 = new BonbonDTO();
        assertThat(bonbonDTO1).isNotEqualTo(bonbonDTO2);
        bonbonDTO2.setId(bonbonDTO1.getId());
        assertThat(bonbonDTO1).isEqualTo(bonbonDTO2);
        bonbonDTO2.setId(2L);
        assertThat(bonbonDTO1).isNotEqualTo(bonbonDTO2);
        bonbonDTO1.setId(null);
        assertThat(bonbonDTO1).isNotEqualTo(bonbonDTO2);
    }
}
